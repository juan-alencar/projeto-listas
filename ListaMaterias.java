
public class ListaMaterias {
	
	private NoMaterias prim;
	private NoMaterias ult;
	private int quantiNo;
	
	public ListaMaterias() {
		this.prim = null;
		this.ult = null;
		this.quantiNo = 0;
	}

	public NoMaterias getPrim() {
		return prim;
	}

	public void setPrim(NoMaterias prim) {
		this.prim = prim;
	}

	public NoMaterias getUlt() {
		return ult;
	}

	public void setUlt(NoMaterias ult) {
		this.ult = ult;
	}

	public int getQuantiNo() {
		return quantiNo;
	}

	public void setQuantiNo(int quantiNo) {
		this.quantiNo = quantiNo;
	}
	
	//---------------------------------------------------------------------a partir daqui fui no flow
	
	
	
	public void inserirPrimeiro (Materia m) {
		NoMaterias novoNo = new NoMaterias(m);
		if(this.eVazia()) {
			this.ult = novoNo;
		}
		novoNo.setProx(this.prim);
		this.prim = novoNo;
		this.quantiNo++;
	}
	
	public void inserirUltimo (Materia m) {
		NoMaterias novoNo = new NoMaterias(m);
		if(eVazia()) {
			this.prim = novoNo;
		} else {
			this.ult.setProx (novoNo);
		}
		this.ult = novoNo;
		this.quantiNo++;
	}
	
	public boolean removerNo(String nomeMateria) {
		NoMaterias atual = this.prim;
		NoMaterias ant   = null;
		if(eVazia()) {
			return false;
		} 
		else {
			while((atual != null) && (!atual.getMateria().getNomeMateria().equals(nomeMateria))) {
				ant = atual;
				atual = atual.getProx();
			}
			if(atual == this.prim) {
				if(this.prim == this.ult) {
					this.ult = null;
				}
				this.prim = this.prim.getProx();
			} 
			else {
				if(atual == this.ult) {
					this.ult = ant;
				}
				ant.setProx(atual.getProx());
			}
			this.quantiNo--;
			return true;
		}
	}		//------------------------------------------------------------------
	
	public String pesquisarNo (String nomeMateria) {
		String msg="";
		NoMaterias atual = this.prim;
		while((atual != null)&& (!atual.getMateria().getNomeMateria().equals(nomeMateria))) {
			atual = atual.getProx();
		}
		return msg = "Nome: " + atual.getMateria().getNomeMateria()+"\n"+
					 "Nota: " + atual.getMateria().getNota();
	}
	public String imprimirLista() {
		
		String msg="";
		if(eVazia()) {
			msg="A lista est� vazia!";
		}
		else {
			NoMaterias atual = this.prim;
			while(atual != null) {
					msg += atual.getMateria().getNomeMateria() + " | Nota: " + atual.getMateria().getNota() + "\n";
					atual = atual.getProx();
			}
		}
			return msg;
	}
	
	public boolean eVazia() {
		return (this.prim == null);
	}	
}
