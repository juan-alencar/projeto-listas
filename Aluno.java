
public class Aluno implements Comparable<Aluno> {
	String nome;
	String rgm;
	ListaMaterias materia;
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRgm() {
		return rgm;
	}
	public void setRgm(String rgm) {
		this.rgm = rgm;
	}
	public ListaMaterias getMateria() {
		return materia;
	}
	public void setListaMaterias(ListaMaterias materia) {
		this.materia = materia;
	}
	public int compareTo(Aluno o) {
		int estado = -1;
		if(this.rgm.compareTo(o.rgm) == 0)
			estado = 0;
		else if(this.rgm.compareTo(o.rgm) > 0)
			return 1;
		return estado;
	}
	
}