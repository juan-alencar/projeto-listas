
public class NoMaterias {

	private Materia m;
	private NoMaterias prox;
	
	public NoMaterias(Materia m1) {
		this.m = m1;
		this.prox = null;
	}
	public Materia getMateria() {
		return m;
	}
	public void setMateria(Materia m) {
		this.m = m;
	}
	public NoMaterias getProx() {
		return prox;
	}
	public void setProx(NoMaterias prox) {
		this.prox = prox;
	}
}