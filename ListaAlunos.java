
public class ListaAlunos {
	Aluno[] alunos = new Aluno[60];
	int tamanho = 0;
		
	public boolean isVazia() {
		return (tamanho == 0);
	}
	public boolean isCheia() {
		return (tamanho == alunos.length);
	}
	public int tamLista() {
		return tamanho;
	}
	public Aluno getElemento(int pos) {
		if((pos >= tamanho) || (pos < 0))
			return null;
		return alunos[pos];
	}
	public boolean compara(Aluno a1, Aluno a2) {
		return((a1.nome.equals(a2.nome)) && (a1.rgm.equals(a2.rgm)));
	}
	public int getPos(Aluno aluno) {
		for(int i = 0; i < tamanho; i++)
			if(compara(alunos[i], aluno))
				return i;
		return -1;
	}
	public void deslocaDireita(int pos) {
		for(int i = tamanho; i > pos; i--)
			alunos[i] = alunos[i - 1];
	}
	
	public boolean inserirAlunoOrdenado(Aluno a1) {
		int pos = 0;
		if(isCheia())
			return false;
		if(isVazia()) {
			alunos[0] = a1;
			tamanho++;
			return true;
		} 
		else {
			while((alunos[pos]) != null && ((alunos[pos].compareTo(a1)) <= 0)) {
				if((alunos[pos].compareTo(a1)) == 0) {
					System.out.println("RGM inv�lido! J� est� em uso.");
					return false;
				}
				pos += 1;
			}
		}
		deslocaDireita(pos);
		alunos[pos] = a1;
		tamanho++;
		return true;
	}

	public void deslocaEsquerda(int pos) {
		for(int i = pos; i < (tamanho - 1); i++)
			alunos[i] = alunos[i + 1];
	}
	public boolean removerAluno(int pos) {
		if((pos >= tamanho) || (pos < 0))
			return false;
		deslocaEsquerda(pos);
		tamanho--;
		return true;
	}
	public void exibirLista() {
		if(isVazia())
			System.out.println("A lista est� vazia!");
		for(int i = 0; i < tamanho; i++) {
			
			System.out.println("\nAluno " +(i+1) + "\nNome: " + alunos[i].nome + "\nRGM: " + alunos[i].rgm + "\nDisciplinas:");  
			
			if(alunos[i].materia != null) {
				System.out.println(alunos[i].materia.imprimirLista());
			}
			else {
				System.out.println("Nenhuma disciplina cadastrada.");
			}
		}
		
		
	}

//busca binaria
	public int buscaBinaria(Aluno buscado) {
		
		int inicio = 0;										// inicializando as var		
		int fim = alunos.length - 1;
		int meio;
		int pos_buscado = 0;							
		
		while(inicio <= fim) {								// while fazendo a busca
			meio = ((inicio + fim)/2);				// definindo o meio				
			
			if ((alunos[meio]) != null && (alunos[meio].compareTo(buscado)) == 0) {		// comparando o meio com o rgm buscado
				pos_buscado = meio;							// se for igual, ele guarda o indice meio na var posicao buscado
				return pos_buscado;							// e para o programa			
			}
			else if((alunos[meio]) != null && (alunos[meio].compareTo(buscado)) < 0){	// caso o buscado seja maior que o meio
				inicio = meio + 1;							// ele coloca o inicio como meio+1 para fazer outra comparacao
			}
			else {											//caso ele seja menor
				fim = meio - 1;								// diminui o final para meio - 1, e assim sucessivamente 		
			}
		}
		return -1;        							// por fim ele retorna o indice do rgm buscado
	}
}

