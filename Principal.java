import java.util.*;


public class Principal {
	
	private static void clearBuffer(Scanner ler) {
        if (ler.hasNextLine()) {
            ler.nextLine();
        }
	}

	public static char menu() {
		String msg;
		System.out.println("---------------------------");
		System.out.println("     Escolha uma op��o     ");
		System.out.println("---------------------------");
		System.out.println("[1] Matricular aluno");	
		System.out.println("[2] Buscar aluno");	
		System.out.println("[3] Cancelar matr�cula");
		System.out.println("[4] Imprimir lista");
		System.out.println("[5] Cadastrar disciplina");
		System.out.println("[0] Sair do programa");
		System.out.println("---------------------------");
		msg = new Scanner(System.in).next();
		return msg.charAt(0);
	}
	
	public static void main(String[] args) {
		Scanner ler = new Scanner(System.in);
		ListaAlunos listaSeq = new ListaAlunos();
		char op;
		int op2;
		
		do {
			op = menu();
		
			switch(op) {
				case '1':	//Inserir Aluno
					System.out.println("---Inserindo Aluno---");
					
					Aluno a = new Aluno();
					System.out.print("Nome aluno: ");
					a.setNome(ler.nextLine());
					System.out.print("RGM: ");
					a.setRgm(ler.nextLine());
					
//					listaSeq.inserirAlunoOrdenado(a);
					if (listaSeq.inserirAlunoOrdenado(a)) {
					
						System.out.println("Deseja matricular alguma disciplina? [1-> sim || 2-> n�o]");
						
						op2 = ler.nextInt();
	                	clearBuffer(ler);
	                	
	                	while(op2 == 1) {
	                    	Materia m = new Materia();
	                    	System.out.println("Disciplina: ");                        	
	                    	m.setNomeMateria(ler.nextLine());
	                    	System.out.println("Nota: ");                        	
	                    	m.setNota(ler.nextDouble());
	                    	clearBuffer(ler);
	                    	
	                    	if(listaSeq.alunos[listaSeq.buscaBinaria(a)].materia == null) {
	                    		ListaMaterias listaEnc = new ListaMaterias();
	                    		listaSeq.alunos[listaSeq.buscaBinaria(a)].setListaMaterias(listaEnc);
	                    		listaEnc.inserirPrimeiro(m);
	//                    		System.out.println(listaEnc.imprimirLista());
	                    		
	                    	}
	                    	else {
	                    		listaSeq.alunos[listaSeq.buscaBinaria(a)].materia.inserirUltimo(m);
	//                    		System.out.println(listaSeq.alunos[listaSeq.buscaBinaria(a)].materia.imprimirLista());
	                    	}
	                    	
	                    	System.out.println("Deseja matricular outra disciplina? [1-> sim || 2-> n�o]");
	                    	op2 = ler.nextInt();
	                    	clearBuffer(ler);
	                	}
	                }
			
					break;	
//-----------------------------------------------------------------------------------------------------------------------
				case '2':	//Buscar Aluno
					if(listaSeq.isVazia()) {
						System.out.println("N�o h� registro de nenhum aluno cadastrado.");
					} 
					else {
						System.out.print("---Localizando Aluno---\nDigite o RGM: ");
						Aluno buscado = new Aluno();
						buscado.setRgm(ler.nextLine());

						if(listaSeq.buscaBinaria(buscado) == -1) {
							System.out.println("O RGM procurado n�o est� na lista!");
						} 
						else {
							int index = listaSeq.buscaBinaria(buscado);
							System.out.println("::::::ENCONTRADO::::::::");
							System.out.println("Nome:" + listaSeq.alunos[index].nome);
							System.out.println("RGM: " + listaSeq.alunos[index].rgm);							
							System.out.println("Disciplinas: \n" + listaSeq.alunos[index].materia.imprimirLista());
							System.out.println(":::::::::::::::::::::::::");							
						}
					}
					break;
//-----------------------------------------------------------------------------------------------------------------------	
				case '3':	//Cancelar Matr�cula
					if(listaSeq.isVazia()) {
						System.out.println("N�o h� alunos matriculados.");
					} 
					else {
						System.out.println("---Cancelar matr�cula---\nDigite o RGM:");
						Aluno buscado = new Aluno();
						buscado.setRgm(ler.nextLine());

						if(listaSeq.buscaBinaria(buscado) == -1) {
							System.out.println("O RGM procurado n�o est� na lista!");
						}
						else {
							System.out.println("Nome: " + listaSeq.alunos[listaSeq.buscaBinaria(buscado)].nome);
							System.out.println("Deseja mesmo cancelar essa matr�cula? [1-> sim || 2-> n�o]");
							op2 = ler.nextInt();
							clearBuffer(ler);
							
							if(op2  == 1) {
								listaSeq.removerAluno((listaSeq.buscaBinaria(buscado)));
								System.out.println("---Apagado com Sucesso---\n");
								System.out.println("---Lista Atualizada---");
								listaSeq.exibirLista();
								System.out.println("----------------------");
							}
						}	
					}
					break;
//-----------------------------------------------------------------------------------------------------------------------	
				case '4':	//Exibir Lista
					System.out.println("--------------------------");
					listaSeq.exibirLista();
					System.out.println("--------------------------");
					System.out.println("Total de alunos matriculados: " + listaSeq.tamLista());
					break;
					
//-----------------------------------------------------------------------------------------------------------------------					
				case '5':	//Cadastrar disciplina
					System.out.println("---Cadastrando disciplina---");						
					
					if(listaSeq.isVazia()) {
                        System.out.println("N�o h� alunos matriculados.");
                    } 
					else {
                    	System.out.print("RGM para cadastrar disciplina: ");
                        Aluno buscado = new Aluno();
                        buscado.setRgm(ler.nextLine());
                        
                        if(listaSeq.buscaBinaria(buscado) == -1) {
                            System.out.println("O RGM procurada n�o est� na lista!");
                        } 
                        else {
                        	do{
	                        	int index = listaSeq.buscaBinaria(buscado);
	                        	Materia m = new Materia();
	                        	System.out.println("Disciplina: ");                        	
	                        	m.setNomeMateria(ler.nextLine());
	                        	System.out.println("Nota: ");                        	
	                        	m.setNota(ler.nextDouble());
	                        	clearBuffer(ler);
	                        	
	                        	if(listaSeq.alunos[index].materia == null) {
	                        		ListaMaterias listaEnc = new ListaMaterias();
	                        		listaSeq.alunos[index].setListaMaterias(listaEnc);
	                        		listaEnc.inserirPrimeiro(m);
	                        		System.out.println(listaEnc.imprimirLista());
	                        	} 
	                        	else {
	                        		listaSeq.alunos[index].materia.inserirUltimo(m);
	                        	}
	                        	
                        		System.out.println("Deseja matricular outra disciplina? [1-> sim || 2-> n�o]");
                        		op2 = ler.nextInt();
                        		clearBuffer(ler);
                        		
                        	}while(op2 == 1);                         
                        }
                    }
					break;
					
//-----------------------------------------------------------------------------------------------------------------------	
				case '0':
					System.out.println("---Fim do programa---");
					break;	
//-----------------------------------------------------------------------------------------------------------------------	
				default:
					System.out.println("---Op��o inv�lida, tente novamente!---");
			}
		} while(op != '0');
	}
}
	
	
	
	
